# ipdeaders

> In the present state of affairs the laws that regulate prostitution in India is Immoral Trafficking (Prevention) Act, 1956

> it does not criminalise prostitution per se, and secondly, it punishes the acts of third parties such as middle men, brothel keepers, pimps, etc. who either facilitate this entire act or procure and live on the earnings of the prostitute workers. Since involvement in this sex trade makes the sex workers highly vulnerable to exploitation, hence the latter category of protection is held in a very high regard by the lawmakers.

> sex workers cannot solicit in public spheres but can practice their trade privately. In private spaces neither the workers nor the clients are held criminally liable or prosecuted.

> a major problem arises when even organised prostitution is not allowed.

>  Secondly, since these sex workers do not come under the ambit of labour laws, therefore they are not provided with adequate protection under the said laws.

>  Since the law does not recognise prostitution as a profession, therefore it is not possible to take such unfavourable clients to court. In the current system, prostitutes are not considered as bearers of rights. They are highly pimped and often raped. Most of the girls involved in this profession are forced into it. Such innocent and unsuspected women generally are forced into this sex trade before attaining the age of eighteen years. It is estimated that every hour, with four women and girls entering into prostitution in India, three of them do so against their will. After being sold, they get trapped into shady brothels, raped and forced to sleep or have unprotected sex with psychopaths who burn and bruise them. Anyone who tries to escape these prisons is brought back by use of force and tortured even more so as to set a deterrent example for other workers. Till the time they continue to serve in the brothels they are denied of all basic rights, for example, they neither do they have ration cards nor do they have a right to vote. They are forced to live in poverty and miserable living conditions. Lack of regulation and periodical medical tests causes the rampant spread of sexually transmitted diseases like HIV-AIDS. These brothel turned dungeons not only infects them with STDs but also other diseases such as cervical cancer or traumatic brain injury or psychological disorders etc. which do not find their cure for a very long period of time. After they grow old, they are thrown on the streets accompanied with no means to earn a shelter and bread and butter for rest of their lives. Being suppressed by societal norms and notions of morality, their voices remain unheard. This problem gets aggravated to a completely different level when these prostitutes are not even aware of the rights that are available to them.

> The Immoral Trafficking (Prevention) Act (ITPA) 1986 although aims at removing the middlemen from this profession but its practical implementation has resulted in depriving the sex workers of their means of earning a livelihood

> he term public solicitation has been interpreted vaguely and as a result police officials have been known to accuse workers of solicitation and then demand bribes or free sex.

# SexualRightsInitiative.com

> SANGRAM found that less than 2% of the wo
men have been to school of any kind, and
less than 50% of that 2% have finished high school.

# data.unaids.org

> For instance the initial baseline survey of the community in Sangli in 1992, done by SANGRAM showed that less than 2% of the women have ever been to school of any kind and less than 50% of this 2% reached high school.

# www.tribuneindia.com

> In ancient India, there was a practice of the rich asking Nagarvadhu to sing and dance, noted in history as "brides of the town". Famous examples include Amrapali, state courtesan and Buddhist disciple, described in "Vaishali Ki Nagarvadhu" by Acharya Chatursen and Vasantasena, a character in the classic Sanskrit story of Mricchakatika, written in the 2nd century BC by Śūdraka. Women competed to win the title of a Nagarvadhu, and it was not considered a taboo
